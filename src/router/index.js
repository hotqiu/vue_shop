import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import welcome from '../components/welcome.vue'
import user from '../components/zujian/user.vue'
import rights from '../components/power/rights.vue'
import roles from '../components/power/roles.vue'
import cate from '../components/goods/cate.vue'
import params from '../components/goods/params.vue'
import goods from '../components/goods/cass.vue'
import add from '../components/goods/add.vue'
import bianji from '../components/goods/bianji.vue'
import order from  '../components/order/order.vue'
import report from '../components/report/report.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/', redirect: '/login'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/home',
    component: Home,
    redirect:'/welcome',
    children:[{
      path: '/welcome',
      component: welcome,
    },
  {
    path: '/users',
    component: user,
  },
{
    path: '/rights',
    component: rights,
},{
  path: '/roles',
  component: roles,
},{
  path:'/categories',
  component:cate
},
{
  path:'/params',
  component:params
},{
  path:'/goods',
  component:goods,
},
{
  path:'/goods/add',
  component:add,
},
{
  path:'/goods/goods',
  redirect: '/goods'
},
{
  path:'/goods/users',
  redirect:'/users'
},{
  path:'/goods/rights',
  redirect:'/rights'
},{
  path:'/goods/roles',
  redirect:'/roles'
},
{
  path:'/goods/categories',
  redirect:'/categories'
},
{
  path:'/goods/params',
  redirect:'/params'
},
{
  path:'/goods/bianji',
  component:bianji
},
{
  path:'/orders',
  component:order
},{
  path:'/reports',
  component:report
}

]
  }

]

const router = new VueRouter({
  routes
})

//挂载路由组件导航卫士
router.beforeEach((to, from, next) => {
  //to 将要访问的路径
  //from 代表从哪个路径跳转而来
  //next 表示放行   next('/xxx')强制跳转
  if (to.path === "/login") return next();
  //从sessionstorage中获取到要报存的token值
  const str = window.sessionStorage.getItem('token')
  //没有token，直接跳转到登录页
  if (!str) return next('/login')
  next()
})
export default router
