import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
//导入全局样式表，设置body高度和html,app高度为100%，否则在后面使用%设置宽度时不显示
import'./assets/css/gobal.css'
// 导入字体图标
import './assets/fonts/iconfont.css'
//加载axios
import axios from 'axios'
import tree from 'vue-table-with-tree-grid'
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme


//配置请求的根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
//给其他api提供token
axios.interceptors.request.use(config=>{
  console.log(config);
  // 固定写法，必须return config
  config.headers.Authorization=window.sessionStorage.getItem('token')
  return config
})
Vue.prototype.$http = axios
//在这里挂载完axios后可以在组件里面通过this。$http获取axios,不必在每个页面都进行加载
Vue.component('tree-table',tree)
Vue.use(VueQuillEditor, /* { default global options } */)





Vue.config.productionTip = false
Vue.filter('dateFormat',function(originVal){
  const dt = new Date(originVal)
  const y = dt.getFullYear()
  const m = (dt.getMonth()+1+'').padStart(2,'0')
  const d = (dt.getDate()+'').padStart(2,'0')

  const hh = (dt.getHours()+'').padStart(2,'0')
  const mm = (dt.getMinutes()+'').padStart(2,'0')
  const ss = (dt.getSeconds()+'').padStart(2,'0')

  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

new Vue({
  router,
  render: h => h(App),
  beforeCreate(){
    Vue.prototype.$bus = this
    
  },
}).$mount('#app')
