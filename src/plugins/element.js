import Vue from 'vue'
import {
    Form,
    FormItem,
    Button,
    Input,
    Message,
    menu,
    submenu,
    menuItemGroup,
    menuItem,
    card,
    row,
    col,
    table,
    tableColumn,
    Switch,
    tooltip,
    pagination,
    dialog,
    MessageBox,
    tag,
    tree,
    Select,
    Option,
    Cascader,
    Alert,
    Tabs,
    tabPane,
    steps,
    step,
    Checkbox,
    CheckboxGroup,
    upload


} from 'element-ui'
//message为导入弹框提示组件



Vue.use(upload)
Vue.use(Checkbox)
Vue.use(CheckboxGroup)
Vue.use(step)
Vue.use(steps)
Vue.use(tabPane)
Vue.use(Tabs)
Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(menu)
Vue.use(submenu)
Vue.use(menuItemGroup)
Vue.use(menuItem)
Vue.use(card)
Vue.use(row)
Vue.use(col)
Vue.use(table)
Vue.use(tableColumn)
Vue.use(Switch)
Vue.use(tooltip)
Vue.use(pagination)
Vue.use(dialog)
Vue.use(tag)
Vue.use(tree)
Vue.use(Select)
Vue.use(Option)
Vue.use(Cascader)
Vue.use( Alert)
Vue.prototype.$message = Message//加入到全局组件vue中，方便个主键进行调用
Vue.prototype.$confirm = MessageBox.confirm
